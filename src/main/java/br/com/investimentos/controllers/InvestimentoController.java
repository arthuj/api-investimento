package br.com.investimentos.controllers;

import br.com.investimentos.models.Investimento;
import br.com.investimentos.models.Simulacao;
import br.com.investimentos.models.dtos.SimulacoesDTO;
import br.com.investimentos.repositories.InvestimentoRepository;
import br.com.investimentos.services.InvestimentoService;
import br.com.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;
    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public ResponseEntity<Iterable<Investimento>> buscarTodosInvestimentos()
    {
        Iterable<Investimento> investimentos = investimentoService.buscarTodosInvestimentos();
        return ResponseEntity.status(200).body(investimentos);
    }

    @PostMapping
    public ResponseEntity<Investimento> salvarInvestimento(@Valid @RequestBody Investimento investimento)
    {
        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarInvestimento(@PathVariable int id)
    {
        try {
            investimentoService.removerInvestimento(id);
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping({"/{id}/simulacao"})
    public SimulacoesDTO salvarSimulacao(@PathVariable int id, @RequestBody @Valid Simulacao simulacao)
    {
        try {
            SimulacoesDTO simulacoesDTO = simulacaoService.salvarSimulacao(id,simulacao);
            return simulacoesDTO;
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}

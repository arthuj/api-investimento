package br.com.investimentos.controllers;

import br.com.investimentos.models.Simulacao;
import br.com.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public ResponseEntity<Iterable<Simulacao>> retornarSimulacoes()
    {
        Iterable<Simulacao> simulacoes = simulacaoService.retornarSimulacao();
        return ResponseEntity.status(200).body(simulacoes);
    }
}

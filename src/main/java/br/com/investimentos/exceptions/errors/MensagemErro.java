package br.com.investimentos.exceptions.errors;

import java.util.HashMap;

public class MensagemErro {

    private String erro;
    private String mensagemDeErro;
    private HashMap<String, ObjetoErro> camposDeErro;

    public MensagemErro(String erro, String mensagemDeErro, HashMap<String, ObjetoErro> camposDeErro) {
        this.erro = erro;
        this.mensagemDeErro = mensagemDeErro;
        this.camposDeErro = camposDeErro;
    }

    public MensagemErro() {
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String, ObjetoErro> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ObjetoErro> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}

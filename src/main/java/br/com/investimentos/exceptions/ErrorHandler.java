package br.com.investimentos.exceptions;

import br.com.investimentos.exceptions.errors.MensagemErro;
import br.com.investimentos.exceptions.errors.ObjetoErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    @Value("${mensagem.padrao.de.validacao}")
    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public MensagemErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {
        HashMap<String, ObjetoErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        // Lista de erros
        List<FieldError> fieldErrors = resultado.getFieldErrors();

        for (FieldError erro : fieldErrors) {
            erros.put(erro.getField(), new ObjetoErro(erro.getDefaultMessage(),
                    erro.getRejectedValue().toString()));
        }

        MensagemErro mensagemDeErro = new MensagemErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                mensagemPadrao, erros);
        return mensagemDeErro;
    }

}



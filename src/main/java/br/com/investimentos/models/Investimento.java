package br.com.investimentos.models;


import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nome;
    @NotNull(message = "Rendimento é obrigatório")
    private double rendimentoAoMes;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Simulacao> simulacaoList;


    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public List<Simulacao> getSimulacaoList() {
        return simulacaoList;
    }

    public void setSimulacaoList(List<Simulacao> simulacaoList) {
        this.simulacaoList = simulacaoList;
    }

}

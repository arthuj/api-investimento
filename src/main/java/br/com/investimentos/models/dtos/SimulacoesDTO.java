package br.com.investimentos.models.dtos;

import javax.validation.constraints.Digits;

public class SimulacoesDTO {

    private double rendimentoPorMes;
    private  double valorMontante;

    public SimulacoesDTO() {
    }



    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getValorMontante() {
        return valorMontante;
    }

    public void setValorMontante(double valorMontante) {
        this.valorMontante = valorMontante;
    }
}

package br.com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull(message = "Nome é obrigatório")
    private String nomeInteressado;
    @Email(message = "O email não é válido")
    private String email;
    @NotNull(message = "Por favor preencher o valor")
    @Digits(integer = 10, fraction = 2, message = "Valor tem mais de 2 números após a vírgula, ou maior que 10 digítos")
    private double valorAplicado;
    @NotNull(message = "Número de meses é obrigatório")
    private int numeroDeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;


    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getNumeroDeMeses() {
        return numeroDeMeses;
    }

    public void setNumeroDeMeses(int numeroDeMeses) {
        this.numeroDeMeses = numeroDeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nome) {
        this.nomeInteressado = nome;
    }
}

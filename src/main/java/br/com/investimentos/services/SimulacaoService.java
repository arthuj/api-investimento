package br.com.investimentos.services;

import br.com.investimentos.models.Investimento;
import br.com.investimentos.models.Simulacao;
import br.com.investimentos.models.dtos.SimulacoesDTO;
import br.com.investimentos.repositories.InvestimentoRepository;
import br.com.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;
    @Autowired
    InvestimentoRepository investimentoRepository;

    public SimulacoesDTO  salvarSimulacao(int id,Simulacao simulacao)
    {
        SimulacoesDTO simulacoesDTO = new SimulacoesDTO();
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()) {
            simulacao.setInvestimento(optionalInvestimento.get());
            simulacaoRepository.save(simulacao);
            simulacoesDTO = calcularSimulacao(simulacao);

            return simulacoesDTO;
        }
        throw new RuntimeException("O id não foi encontrado");

    }

    private SimulacoesDTO calcularSimulacao(Simulacao simulacao) {
        SimulacoesDTO simulacoesDTO = new SimulacoesDTO();
        double montanteFinal = simulacao.getValorAplicado();
        simulacoesDTO.setRendimentoPorMes(simulacao.getInvestimento().getRendimentoAoMes() + 1);
        for(int i = 1; i <= simulacao.getNumeroDeMeses(); i++)
        {
            montanteFinal *= simulacoesDTO.getRendimentoPorMes();
        }
        simulacoesDTO.setValorMontante(montanteFinal);
        return simulacoesDTO;
    }

    public Iterable<Simulacao> retornarSimulacao()
    {
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return  simulacoes;
    }




}

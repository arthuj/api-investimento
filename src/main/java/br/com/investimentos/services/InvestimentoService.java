package br.com.investimentos.services;

import br.com.investimentos.models.Investimento;
import br.com.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    public Iterable<Investimento> buscarTodosInvestimentos()
    {
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Investimento criarInvestimento(Investimento investimento)
    {
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public void removerInvestimento(int id)
    {
        if(investimentoRepository.existsById(id))
            investimentoRepository.deleteById(id);
        else
            throw new RuntimeException("O lead não foi encontrado");
    }


}

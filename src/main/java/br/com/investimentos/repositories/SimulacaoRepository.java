package br.com.investimentos.repositories;

import br.com.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao,Integer> {
}
